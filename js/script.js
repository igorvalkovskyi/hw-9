const tabsName = document.querySelectorAll(".tabs-title");
const contents = document.querySelectorAll(".content");

for (let i = 0; i < tabsName.length; i++) {
	tabsName[i].addEventListener("click", ( event ) => {

        let tabsCurrent = event.target.parentElement.children;
        for( let t = 0; t < tabsCurrent.length; t++ ){
            tabsCurrent[t].classList.remove("active");
        }
        tabsName[i].classList.add("active");

        let contentsCurrent = event.target.parentElement.nextElementSibling.children;
        for( let c = 0; c < contentsCurrent.length; c++ ){
            contentsCurrent[c].classList.remove("content-active");
        }
        contents[i].classList.add("content-active");
    });
}


  